package vodafone.tools.database.oracle.connection;

import vodafone.tools.database.oracle.request.Request;
import vodafone.tools.database.oracle.statement.StatementImpl;

import java.io.IOException;
import java.sql.SQLException;

public interface Connector {

    static Connector connect(String databaseURL, String username, String password) throws SQLException {
        return new ConnectorImpl().connect(databaseURL, username, password);
    }

    static Connector connect(String propertiesName) throws SQLException, IOException {
        return new ConnectorImpl().connect(propertiesName);
    }

    Request createRequest();

    StatementImpl createStatement();

    void close() throws SQLException;
}
