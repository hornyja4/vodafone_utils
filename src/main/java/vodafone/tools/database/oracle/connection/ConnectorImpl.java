package vodafone.tools.database.oracle.connection;

import vodafone.tools.database.oracle.request.Request;
import vodafone.tools.database.oracle.request.RequestImpl;
import vodafone.tools.database.oracle.statement.StatementImpl;
import vodafone.tools.database.shared.properties.DatabaseProperties;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectorImpl implements Connector {
    private Connection connection;

    private void checkOracleDriver() {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not find 'oracle.jdbc.driver.OracleDriver'.");
        }
    }

    Connector connect(String databaseURL, String username, String password) throws SQLException {
        checkOracleDriver();
        connection = DriverManager.getConnection(databaseURL, username, password);
        return this;
    }

    Connector connect(String propertiesName) throws SQLException, IOException {
        Properties properties = DatabaseProperties.loadProperties(propertiesName);
        return connect(properties.getProperty("url"), properties.getProperty("username"), properties.getProperty("password"));
    }

    @Override
    public Request createRequest() {
        return new RequestImpl(connection);
    }

    @Override
    public StatementImpl createStatement() {
        return new StatementImpl(connection);
    }

    public void close() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }
}

