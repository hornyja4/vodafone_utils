package vodafone.tools.database.oracle.statement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class StatementImpl implements IStatement {
    private Connection connection;

    public StatementImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Statement statement() throws SQLException {
        return connection.createStatement();
    }

    @Override
    public PreparedStatement prepareStatement(String statement) throws SQLException {
        return connection.prepareStatement(statement);
    }
}
