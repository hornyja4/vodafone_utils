package vodafone.tools.database.oracle.statement;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public interface IStatement {
    PreparedStatement prepareStatement(String statement) throws SQLException;
    Statement statement() throws SQLException;
}