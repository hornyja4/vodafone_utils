package vodafone.tools.database.oracle.request;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface Request {
    ResultSet executeQuery(String query) throws SQLException;
}
