package vodafone.tools.database.oracle.request;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class RequestImpl implements Request {
    private Connection connection;

    public RequestImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public ResultSet executeQuery(String query) throws SQLException {
        return connection.createStatement().executeQuery(query);
    }
}
