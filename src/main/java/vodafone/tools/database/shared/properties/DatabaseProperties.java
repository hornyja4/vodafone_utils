package vodafone.tools.database.shared.properties;

import java.io.IOException;
import java.util.Properties;

public interface DatabaseProperties {
    Properties properties = new Properties();

    static Properties loadProperties(String propertiesName) throws IOException {
        properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(propertiesName));
        return properties;
    }
}
