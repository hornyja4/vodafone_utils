package vodafone.tools.database.shared.sql;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

@XmlRootElement
public class SqlMap {
    Map<String, String> sqls = new HashMap<>();

    public Map<String, String> getSqls() {
        return sqls;
    }

    public void setSqls(Map<String, String> sqls) {
        this.sqls = sqls;
    }

    public String getSql(Enum queries) {
        return sqls.get(queries.name());
    }

    public static SqlMap load(String sqlFile) {
        return createObjectFromXML(sqlFile);
    }

    private static SqlMap createObjectFromXML(String sqlFile) {
        System.setProperty("javax.xml.accessExternalDTD", "all");
        File XMLFile = findFile(new File("src/main/resources"), sqlFile);
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(SqlMap.class);
            return (SqlMap) jaxbContext.createUnmarshaller().unmarshal(XMLFile);
        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }
    }

    private static File findFile(File folder, String fileName) {
        File foundFile = null;
        if (folder.isDirectory()) {
            for (String file : folder.list()) {
                File temp = findFile(new File(folder, file), fileName);
                if ((temp != null) && (temp.isFile())) {
                    foundFile = temp;
                }
            }
        } else if (!folder.isHidden()) {
            for (File file : folder.getParentFile().listFiles()) {
                if (file.getName().equals(fileName)) {
                    return file;
                }
            }
        }
        return foundFile;
    }
}
