package vodafone.tools.database.factories;

import vodafone.tools.database.shared.sql.SqlMap;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class SQLFactory {
    private static SqlMap sqlMap;
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.YYYY HH.mm.ss");

    static String campaignSMSQuery(String externalId) {
        loadSqlFile(DatabaseFactory.Tibco.SQL_FILE);
        String statement = sqlMap.getSql(DatabaseFactory.Tibco.Queries.CAMPAIGN_SMS);
        return processTimestamp(processExternalId(statement, externalId));
    }

    static String SCASMSQuery(String orderId) {
        loadSqlFile(DatabaseFactory.Tibco.SQL_FILE);
        String statement = sqlMap.getSql(DatabaseFactory.Tibco.Queries.SCA_SMS);
        return processTimestamp(processOrderId(statement, orderId));
    }

    private static void loadSqlFile(String sqlFile) {
        sqlMap = SqlMap.load(sqlFile);
    }

    private static String processTimestamp(String sql) {
        return processFromTimestamp(processToTimestamp(sql));
    }

    private static String processExternalId(String sql, String externalId) {
        return sql.replaceAll("EXTERNALID_PLACEHOLDER", externalId);
    }

    private static String processOrderId(String sql, String orderId) {
        return sql.replaceAll("ORDERID_PLACEHOLDER", orderId);
    }

    private static String processFromTimestamp(String sql) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MINUTE, -15);
        return sql.replaceAll("FROM_PLACEHOLDER", dateFormat.format(cal.getTime()));
    }

    private static String processToTimestamp(String sql) {
        return sql.replaceAll("TO_PLACEHOLDER", dateFormat.format(new Date()));
    }
}
