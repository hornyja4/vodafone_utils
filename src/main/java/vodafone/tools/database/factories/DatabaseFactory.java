package vodafone.tools.database.factories;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import vodafone.tools.database.oracle.connection.Connector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DatabaseFactory {

    public static class Tibco {
        static final String SQL_FILE = "tibco.xml";

        public static Document getCampaignSMSResponse(String externalId) {
            return executeQuery(SQLFactory.campaignSMSQuery(externalId));
        }

        public static Document getSCASMSResponse(String orderId) {
            return executeQuery(SQLFactory.SCASMSQuery(orderId));
        }

        public enum Queries {
            CAMPAIGN_SMS, SCA_SMS;
        }
    }

    public static class EDW {
        static final String SQL_FILE = "edw.xml";

        public enum Queries {

        }
    }

    private static Document executeQuery(String query) {
        Connector connector = null;
        DocumentBuilderFactory factory;
        DocumentBuilder builder;
        try {
            connector = Connector.connect("oracle.properties");
            ResultSet resultSet = connector.createStatement().statement().executeQuery(query);
            if (resultSet.next()) {
                factory = DocumentBuilderFactory.newInstance();
                builder = factory.newDocumentBuilder();
                return builder.parse(new InputSource(new StringReader(resultSet.getString(1))));
            }
        } catch (SQLException | IOException | ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        } finally {
            if (connector != null) {
                try {
                    connector.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
