package vodafone.tools.api.shared;

public class Constants {

    public static class Rtim {
        public static final String BASE_URL = "http://czrt03vr.vfcz.dc-ratingen.de:8080/artim-decision/rs/interaction/";
        public static final String CREDENTIALS = "Basic bG91aXM6bG91aXM=";
    }
}
