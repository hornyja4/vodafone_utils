package vodafone.tools.api.rtim;

import retrofit2.Call;
import retrofit2.http.*;

public interface RtimConnector {
    String BASE_URL = "./vfcz:{msisdn}/TargetedMessages;channel=siebel;inp=sblNBA?version=2";

    @Headers({
            "User-Agent: User-Agent\t Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:48.0) Gecko/20100101 Firefox/48.0",
            "Accept: application/json",
            "Accept-Encoding: gzip, deflate",
            "Content-Type: application/json",
            "Accept-Language: fr,en-US;q=0.7,en;q=0.3"})
    @POST(BASE_URL)
    Call<MsisdnNbaJSONPojo> rtimDecision(@Header("Authorization") String credentials, @Path("msisdn") String msisdn, @Body RtimPojo rtimPojo);
}
