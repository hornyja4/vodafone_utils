package vodafone.tools.api.rtim;

import com.google.gson.annotations.SerializedName;

/*
 * Mapping of POJO to JSON Object and vice versa
 *
 * JSON Sample to be built/parsed
 * {"extKeys":{
 *   "customerKey":"${msisdn}"},
 *   "tagData":{
 *     "Channel":{
 *       "sblCurrentPageTc":{
 *         "userLoginT":"xivatref",
 *         "userChannelT":"retail"
 *       }
 *     }
 *   }
 * }
 *
 */
class RtimPojo {
    private ExtKeys extKeys;
    private TagData tagData = new TagData();

    public RtimPojo(String msisdn) {
        extKeys = new ExtKeys(msisdn);
    }

    class ExtKeys {
        private String customerKey;

        public ExtKeys(String customerKey) {
            this.customerKey = customerKey;
        }

        public String getCustomerKey() {
            return customerKey;
        }

        public void setCustomerKey(String customerKey) {
            this.customerKey = customerKey;
        }
    }

    class TagData {
        @SerializedName("Channel")
        private Channel channel = new Channel();

        public Channel getChannel() {
            return channel;
        }

        public void setChannel(Channel channel) {
            this.channel = channel;
        }
    }

    class Channel {
        private SblCurrentPageTc sblCurrentPageTc = new SblCurrentPageTc();

        public SblCurrentPageTc getSblCurrentPageTc() {
            return sblCurrentPageTc;
        }

        public void setSblCurrentPageTc(SblCurrentPageTc sblCurrentPageTc) {
            this.sblCurrentPageTc = sblCurrentPageTc;
        }
    }

    class SblCurrentPageTc {
        private String userLoginT = "NBA_VALIDATION_TOOL";
        private String userChannelT = "retail";

        public String getUserLoginT() {
            return userLoginT;
        }

        public void setUserLoginT(String userLoginT) {
            this.userLoginT = userLoginT;
        }

        public String getUserChannelT() {
            return userChannelT;
        }

        public void setUserChannelT(String userChannelT) {
            this.userChannelT = userChannelT;
        }
    }
}
