package vodafone.tools.api.rtim;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings({"unused"})
public class MsisdnNbaJSONPojo {

    @SerializedName("Messages")
    @Expose
    private List<Message> messages = null;

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

}

@SuppressWarnings({"unused"})
class Link {

    @SerializedName("rel")
    @Expose
    private String rel;
    @SerializedName("href")
    @Expose
    private String href;
    @SerializedName("type")
    @Expose
    private String type;

    public String getRel() {
        return rel;
    }

    public void setRel(String rel) {
        this.rel = rel;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}

@SuppressWarnings({"unused"})
class Message {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("activationStart")
    @Expose
    private String activationStart;
    @SerializedName("activationEnd")
    @Expose
    private String activationEnd;
    @SerializedName("link")
    @Expose
    private Link link;
    @SerializedName("objective")
    @Expose
    private String objective;
    @SerializedName("messageClass")
    @Expose
    private String messageClass;
    @SerializedName("msgseq")
    @Expose
    private Integer msgseq;
    @SerializedName("strategy")
    @Expose
    private String strategy;
    @SerializedName("stratseq")
    @Expose
    private Integer stratseq;
    @SerializedName("arbitration")
    @Expose
    private String arbitration;
    @SerializedName("arbvalue")
    @Expose
    private Double arbvalue;
    @SerializedName("value")
    @Expose
    private Double value;
    @SerializedName("msgSblContentTc.extendedMsgT")
    @Expose
    private String msgSblContentTcExtendedMsgT;
    @SerializedName("msgSblContentTc.sblNbaDescT")
    @Expose
    private String msgSblContentTcSblNbaDescT;
    @SerializedName("msgSblContentTc.sblNbaLinkT")
    @Expose
    private String msgSblContentTcSblNbaLinkT;
    @SerializedName("msgSblContentTc.sblNbaNameT")
    @Expose
    private String msgSblContentTcSblNbaNameT;
    @SerializedName("messagePathId")
    @Expose
    private String messagePathId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getActivationStart() {
        return activationStart;
    }

    public void setActivationStart(String activationStart) {
        this.activationStart = activationStart;
    }

    public String getActivationEnd() {
        return activationEnd;
    }

    public void setActivationEnd(String activationEnd) {
        this.activationEnd = activationEnd;
    }

    public Link getLink() {
        return link;
    }

    public void setLink(Link link) {
        this.link = link;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getMessageClass() {
        return messageClass;
    }

    public void setMessageClass(String messageClass) {
        this.messageClass = messageClass;
    }

    public Integer getMsgseq() {
        return msgseq;
    }

    public void setMsgseq(Integer msgseq) {
        this.msgseq = msgseq;
    }

    public String getStrategy() {
        return strategy;
    }

    public void setStrategy(String strategy) {
        this.strategy = strategy;
    }

    public Integer getStratseq() {
        return stratseq;
    }

    public void setStratseq(Integer stratseq) {
        this.stratseq = stratseq;
    }

    public String getArbitration() {
        return arbitration;
    }

    public void setArbitration(String arbitration) {
        this.arbitration = arbitration;
    }

    public Double getArbvalue() {
        return arbvalue;
    }

    public void setArbvalue(Double arbvalue) {
        this.arbvalue = arbvalue;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getMsgSblContentTcExtendedMsgT() {
        return msgSblContentTcExtendedMsgT;
    }

    public void setMsgSblContentTcExtendedMsgT(String msgSblContentTcExtendedMsgT) {
        this.msgSblContentTcExtendedMsgT = msgSblContentTcExtendedMsgT;
    }

    public String getMsgSblContentTcSblNbaDescT() {
        return msgSblContentTcSblNbaDescT;
    }

    public void setMsgSblContentTcSblNbaDescT(String msgSblContentTcSblNbaDescT) {
        this.msgSblContentTcSblNbaDescT = msgSblContentTcSblNbaDescT;
    }

    public String getMsgSblContentTcSblNbaLinkT() {
        return msgSblContentTcSblNbaLinkT;
    }

    public void setMsgSblContentTcSblNbaLinkT(String msgSblContentTcSblNbaLinkT) {
        this.msgSblContentTcSblNbaLinkT = msgSblContentTcSblNbaLinkT;
    }

    public String getMsgSblContentTcSblNbaNameT() {
        return msgSblContentTcSblNbaNameT;
    }

    public void setMsgSblContentTcSblNbaNameT(String msgSblContentTcSblNbaNameT) {
        this.msgSblContentTcSblNbaNameT = msgSblContentTcSblNbaNameT;
    }

    public String getMessagePathId() {
        return messagePathId;
    }

    public void setMessagePathId(String messagePathId) {
        this.messagePathId = messagePathId;
    }

}