package vodafone.tools.api.rtim;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;

import static vodafone.tools.api.shared.Constants.Rtim.BASE_URL;
import static vodafone.tools.api.shared.Constants.Rtim.CREDENTIALS;

public class RtimController {

    public MsisdnNbaJSONPojo sendRequest(String msisdn, RtimPojo rtimPojo) throws IOException {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        RtimConnector rtimConnector = retrofit.create(RtimConnector.class);
        return rtimConnector.rtimDecision(CREDENTIALS, msisdn, rtimPojo).execute().body();
    }
}
